``Euclidian`` is a lightweight 2D and 3D geometry library with no
pretentions to be fast, or especially robust around difficult edge
cases. The objectives are to be easy to use, be quick to learn, and
to be installable without onerous dependencies.



